//
//  main.m
//  Empty Window
//
//  Created by Святослав Щавровский on 17.08.15.
//  Copyright (c) 2015 Святослав Щавровский. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
